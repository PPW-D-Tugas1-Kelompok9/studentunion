from django.urls import re_path,path
from .views import *


#url for app
urlpatterns = [
    path('<int:id>/<str:event_title>/', view_event),
    path('join_event', join_event, name = 'join_event'),
    path('', event_func, name = 'event_page'),
    path('check_user_is_registered', check_user_is_registered, name='check_user_is_registered')
]
