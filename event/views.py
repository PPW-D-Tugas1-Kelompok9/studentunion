from django.shortcuts import render
from .models import eventModel
from django.http import JsonResponse

# Create your views here.
response = {}
def event_func(request):
    return render(request, 'event.html', {'event_attr' : eventModel.objects.all()})

def view_event(request,id,event_title):
    response['event'] = eventModel.objects.get(id=id)
    response['members'] = eventModel.objects.get(id=id).profile_set.all()
    # response['nonmembers'] = eventModel.objects.get(id=id).attendant_nonMember.all()
    return render(request,'see_event.html',response)

def join_event(request):
	if request.user.is_authenticated:
		event_id = request.GET.get('event_id', '')
		query = eventModel.objects.filter(id= event_id)
		if query:
			request.user.profile.events.add(query.get())
			return JsonResponse({"success": True})
	return JsonResponse({"success": False})

def check_user_is_registered(request):
	if request.user.is_authenticated:
		event_id = request.GET.get('event_id', '')
		members = list(eventModel.objects.get(id=event_id).profile_set.all())
		for member in members:
			if member.user.username == request.user.username:
				return JsonResponse({"registered": True})
	return JsonResponse({"registered": False})
