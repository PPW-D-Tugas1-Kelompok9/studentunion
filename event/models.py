from django.db import models
from registerMemberApp.models import memberModel
from registerEvent.models import nonMemberModel

class eventModel(models.Model):
    title = models.CharField(max_length=50)
    description = models.TextField()
    date = models.DateTimeField()
    place = models.CharField(max_length=20)
    image_url = models.CharField(max_length=100)
    attendant_member = models.ManyToManyField(memberModel, blank = True)
    attendant_nonMember = models.ManyToManyField(nonMemberModel, blank = True)
    popular = models.CharField(max_length=5, default= True)
