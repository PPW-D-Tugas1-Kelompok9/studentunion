from django.test import TestCase, Client
from django.urls import reverse, resolve
from django.contrib.auth.models import User
import json
from .views import *
from profileApp.views import Profile
from .models import *
import datetime
from django.utils import timezone

class eventTest (TestCase) :
    def test_event_page_is_exist(self):
        response = Client().get(reverse('event_page'))
        self.assertEqual(response.status_code, 200)

    def test_event_page_using_index_func(self):
        response = resolve(reverse('event_page'))
        self.assertEqual(response.func, event_func)

    def test_event_page_contain_correct_html(self):
        response= Client().get(reverse('event_page'))
        html_response=response.content.decode('utf8')
        self.assertIn("EVENT", html_response)
        self.assertIn("Student Union", html_response)

    def test_join_and_view_event(self):
        user = User.objects.create(username='Riani')
        user.set_password('Riani2712')
        profile = Profile.objects.create(user = user)
        user.save()
        login = self.client.login(username='Riani', password='Riani2712')

        response = self.client.get('/join_event?event_id=1')
        self.assertEqual(json.loads(response.content), {"success": False})
        
        date = datetime.datetime(2018, 11, 20, 20, 8, 7, 127325, tzinfo=timezone.utc)
        event = eventModel.objects.create(title = 'COMPFEST', description = 'Compfest adalah event',  date = date, place='Pacilcom',
            image_url='https://image.ibb.co/cgQiGL/event-1.png', popular=True)

        response = self.client.get('/join_event?event_id=1')
        self.assertEqual(json.loads(response.content), {"success": True})

        response= self.client.get('/1/COMPFEST/')
        html_response=response.content.decode('utf8')
        self.assertIn("COMPFEST", html_response)
        self.assertIn("Riani", html_response)

    def test_user_is_registered_in_an_event(self):
        user = User.objects.create(username='Riani')
        user.set_password('Riani2712')
        profile = Profile.objects.create(user = user)
        user.save()
        login = self.client.login(username='Riani', password='Riani2712')
        
        date = datetime.datetime(2018, 11, 20, 20, 8, 7, 127325, tzinfo=timezone.utc)
        event = eventModel.objects.create(title = 'COMPFEST', description = 'Compfest adalah event',  date = date, place='Pacilcom',
            image_url='https://image.ibb.co/cgQiGL/event-1.png', popular=True)

        response = self.client.get('/check_user_is_registered?event_id=1')
        self.assertEqual(json.loads(response.content), {"registered": False})

        self.client.get('/join_event?event_id=1')

        response = self.client.get('/check_user_is_registered?event_id=1')
        self.assertEqual(json.loads(response.content), {"registered": True})
