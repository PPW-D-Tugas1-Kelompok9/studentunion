from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import resolve, reverse
from django.contrib import auth
from django.http import JsonResponse
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt
from .models import *
from django.core import serializers
from django.http import HttpResponse
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.http import JsonResponse

response = {}

def render_user_event(request):
    if request.user.is_authenticated:
        data = Profile.objects.get(user=request.user)
        list_event = []
        for event in data.events.all():
            event = {'title': event.title, 'description': event.description, 'date': event.date, 'place': event.place, 'image_url': event.image_url}
            list_event.append(event)
        count = len(list_event)
        return JsonResponse({'data': list_event, 'counter' : count})

def view_profile(request):
    if(request.user.is_authenticated):
        try :
            profile = Profile.objects.get(user=request.user)
            if profile :
                response['profile'] = profile.image[0:-6]
            else :
                response['profile'] = static('/img/profile-pic.jpg')
        except :
            response['profile'] = static('/img/profile-pic.jpg')
            #ini harus di test login pake admin
    return render(request,'profile.html',response)

def logout(request):
    print("logout")
    auth.logout(request)
    request.session.flush()
    return HttpResponseRedirect('/')

def check_user(request):
	login = "false"
	if request.user.is_authenticated:
		login = "true"
	return JsonResponse({"login": login})