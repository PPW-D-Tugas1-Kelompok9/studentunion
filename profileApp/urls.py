from django.conf.urls import url
from django.urls import re_path,path
from .views import *

app_name = 'profile'
urlpatterns = [
    path('logout/',logout,name = 'logout'),
    path('check_user', check_user, name = 'check_user'),
    path('render/', render_user_event, name='render'),
    # path('delete/', delete_join_event, name='delete'),
    path('', view_profile, name = 'profilePage'),
]
