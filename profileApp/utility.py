from profileApp.models import Profile

def add_picture(backend, user, response, *args, **kwargs):
    print("nani")
    print(response)
    if not Profile.objects.filter(user=user):
        image = response.get('image', None)
        if image and not image['isDefault']:
            image = image['url']
        else:
            image = None

        user.member = Profile(user=user,
                             image=image)
        user.member.save()
