from importlib import import_module
from django.conf import settings
from django.test import TestCase, Client
from django.urls import resolve
from django.http import request
from .views import *
from .models import *
import json

class SessionTestCase(TestCase):
    def setUp(self):
        settings.SESSION_ENGINE = "django.contrib.sessions.backends.file"
        engine = import_module(settings.SESSION_ENGINE)
        store = engine.SessionStore()
        store.save()
        self.session = store
        self.client.cookies[settings.SESSION_COOKIE_NAME] = store.session_key

class UnitTestProfileApp(SessionTestCase):
    def test_url_profile_page_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_profile_page_using_view_profile_function(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, view_profile)

    def test_profile_page_using_templates(self):
        response = Client().get('/profile/')
        self.assertTemplateUsed(response, 'profile.html')

    def test_url_login(self):
        test = self.client.session
        test['user_id'] = 'studentunion'
        test['name'] = 'studentunion'
        test.save()

        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
    
class profileTest (TestCase) :
    def test_user_is_authenticated(self):
        response = Client().get('/profile/check_user')
        self.assertEqual(json.loads(response.content), {"login": "false"})

        user = User.objects.create(username='Riani')
        user.set_password('Riani2712')
        user.save()
        login = self.client.login(username='Riani', password='Riani2712')

        response = self.client.post('/profile/check_user')
        self.assertEqual(json.loads(response.content), {"login": "true"})

    def test_routing_to_profilePage(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_routing_to_logout(self):
        user = User.objects.create(username='Riani')
        user.set_password('Riani2712')
        user.save()
        login = self.client.login(username='Riani', password='Riani2712')
        
        response = Client().get('/profile/logout/')
        response2 = Client().get('/logout/')
        self.assertEqual(response.status_code, 302)
        
    def test_page_not_found(self):
        response = Client().get('/studentunion')
        self.assertEqual(response.status_code, 404)
