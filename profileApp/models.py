from django.db import models
from django.contrib.auth.models import User
from event.models import eventModel
from django.db.models.signals import post_save
from django.dispatch import receiver

class Profile(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    events = models.ManyToManyField(eventModel, blank = True)
    image = models.CharField(max_length = 200,null=True)
