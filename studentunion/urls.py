"""studentunion URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.urls import re_path
from django.conf.urls import include,url
from registerEvent.views import register
from aboutApp.views import about

urlpatterns = [
    path('admin/', admin.site.urls),
    path('news/',include('newsApp.urls'), name = "news_page"),
    path('registerMember/', include(('registerMemberApp.urls', 'register_member'), namespace='register_member')),
    path('register/', include(('registerEvent.urls', 'register' ), namespace='register')),
    path('profile/',include(('profileApp.urls','profile'),namespace='profile')),
    url(r'^auth/', include('social_django.urls', namespace='social')),
    path('', include('event.urls'), name="event_page"),
    path('about/', include(('aboutApp.urls', 'about'), namespace='about'), name="about"),
    # re_path('',include('mainApp.urls')),

]
