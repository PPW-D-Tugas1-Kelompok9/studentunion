from django.test import TestCase, Client
from datetime import datetime
from django.urls import resolve, reverse
from .views import *
from .models import *

# Create your tests here.
class mainApp_test(TestCase):

    def test_mainApp_url_routing(self):
        response = Client().get('//')
        self.assertEqual(response.status_code, 200)

    # def test_mainApp_correct_view(self):
    #     response = resolve('/')
    #     self.assertEqual(response.func, event_func)

    # def test_mainApp_correct_html_home(self):
    #     response = Client().get('//')
    #     self.assertContains(response, "hello world")
    #     self.assertContains(response,"coming soon")
