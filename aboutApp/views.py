from django.shortcuts import render
import requests
import json
from django.http import JsonResponse
from .models import Testimony

# Create your views here.
def about(request):
    response = {
        'testimony' : Testimony.objects.all()[::-1][0:],
    }
    if request.method == 'POST' and request.is_ajax():
        catched = json.loads(request.body)
        testi = Testimony()
        testi.testimony = catched['testimony']
        testi.save()
        return JsonResponse({'testimony' : 'success!'})
    else:
        return render(request, 'about.html', response)