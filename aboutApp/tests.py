from django.test import TestCase, Client
from django.urls import resolve
from .views import about
import json
# Create your tests here.
class aboutAppTest(TestCase):
    def test_routing_to_about_page(self):
        response = Client().get('/about/')
        self.assertEqual(response.status_code, 200)

    def test_using_about_views(self):
        found = resolve('/about/')
        self.assertEqual(found.func, about)

    def test_ajax_post(self):
        response = self.client.post('/about/', {'testimony':'Mantap ini website bukan kaleng-kaleng. Lanjutkan!'}, content_type='application/json', **{'HTTP_X_REQUESTED_WITH':'XMLHttpRequest'})
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content)
        self.assertEqual(data['testimony'], 'success!')