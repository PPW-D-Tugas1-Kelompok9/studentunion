from django.conf.urls import url
from django.urls import re_path,path
from .views import *

app_name = 'registerEvent'
urlpatterns = [
    # url(r'^$', register, name="register"),
    path('<int:id>/<str:event_name>/nonMember/',register_nonMember,name='register_nonmember'),
    path('<int:id>/<str:event_name>/',register,name='register'),
]
