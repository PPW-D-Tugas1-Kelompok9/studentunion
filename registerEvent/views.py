from django.shortcuts import render, redirect
from .models import nonMemberModel
from .forms import *
from event.models import eventModel
from registerMemberApp.models import memberModel

response = {}
# Create your views here.
def register(request,id,event_name):
    event_model = eventModel.objects.get(id=id)
    response['event'] = event_model

    form_email_validation = RegisterEvent_email_validation(request.POST)
    response['form_email_validation'] = form_email_validation
    
    if form_email_validation.is_valid():
        email = form_email_validation.cleaned_data['email']
        try :
            registered_member = memberModel.objects.get(email=email)
        except :
            registered_member = None

        if(not(registered_member is None)):
            response['member'] = registered_member
            event_model.attendant_member.add(registered_member)
            return redirect('/registerMember/registerSuccess')
        
        else :
            return redirect('./nonMember/')
           

    return render(request,'register.html',response)

def register_nonMember(request,id,event_name):
    event_model = eventModel.objects.get(id=id)
    response['event'] = event_model

    form_nonMember = RegisterEvent_nonMember(request.POST)
    response['form_nonMember'] = form_nonMember

    if form_nonMember.is_valid():
        email = form_nonMember.cleaned_data['email']
        username = form_nonMember.cleaned_data['username']
        password = form_nonMember.cleaned_data['password']
        nonMember = nonMemberModel()
        nonMember.email = email
        nonMember.username = username
        nonMember.password = password
        nonMember.save()
        event_model.attendant_nonMember.add(nonMember)
        return redirect('/registerMember/registerSuccess')
        
    return render(request,'register_nonMember.html',response)