from django import forms

class RegisterEvent_form(forms.Form):
    email =  forms.EmailField(max_length=30, widget=forms.EmailInput(attrs={
        "class":"form-event-input form-control",
        "required":True,
        "placeholder":"Email"
    }))
    username = forms.CharField(max_length=30, widget=forms.TextInput(attrs={
        "class":"form-event-input form-control",
        "required":True,
        "placeholder":"Username"
    }))
    password = forms.CharField(max_length=30, widget=forms.PasswordInput(attrs={
        "class":"form-event-input form-control",
        "required":True,
        "placeholder":"Password"
    }))


class RegisterEvent_email_validation(forms.Form):
    email =  forms.EmailField(max_length=30, widget=forms.EmailInput(attrs={
        "class":"form-event-input form-control",
        "required":True,
        "placeholder":"Email"
    }))

class RegisterEvent_nonMember(forms.Form):
    email =  forms.EmailField(max_length=30, widget=forms.EmailInput(attrs={
        "class":"form-event-input form-control",
        "required":True,
        "placeholder":"Email"
    }))
    username = forms.CharField(max_length=30, widget=forms.TextInput(attrs={
        "class":"form-event-input form-control",
        "required":True,
        "placeholder":"Username"
    }))
    password = forms.CharField(max_length=30, widget=forms.PasswordInput(attrs={
        "class":"form-event-input form-control",
        "required":True,
        "placeholder":"Password"
    }))
