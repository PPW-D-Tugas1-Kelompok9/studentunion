from django.test import TestCase
from django.test import Client
from django.urls import resolve,reverse
from .views import *
from django.utils import timezone
from .models import *
from event.models import eventModel
from .forms import *
# Create your tests here.

class Register_test(TestCase):
    def test_setUp(self):
        return nonMemberModel.objects.create(email = "admin@gmail.com",
                                             username="admin",password="admin123")

    def test_setUp_event(self):
        return eventModel.objects.create(title = "event", description="this is event",date =timezone.now()
                                         ,place = "Fasilkom",image_url ="https://nani.png" )

    def test_url_register_event(self):
        event = self.test_setUp_event()
        response = Client().get('/register/1/this-is-event/')

        self.assertEqual(response.status_code,200)

    def test_url_register_event_nonMember(self):
        event = self.test_setUp_event()
        response = Client().get('/register/1/this-is-event/nonMember/')

        self.assertEqual(response.status_code, 200)

    def test_post_email(self):
        event = self.test_setUp_event()
        response_status = Client().post(reverse('register:register_nonmember',args=[1,"nani"]),
                                        {'email': 'lolol@gmail.com'})
        self.assertEqual(response_status.status_code, 200)

class Register_views_test(TestCase):
    def test_form_is_valid_for_blank_items(self):
        form = RegisterEvent_form(data={'email' : '', 'username' : '', 'password' : ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['email'], ["This field is required."]
        )
        self.assertEqual(
            form.errors['username'], ["This field is required."]
        )
        self.assertEqual(
            form.errors['password'], ["This field is required."]
        )

    def test_form_is_valid_for_blank_items_nonMember(self):
        form = RegisterEvent_nonMember(data={'email' : '', 'username' : '', 'password' : ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['email'], ["This field is required."]
        )
        self.assertEqual(
            form.errors['username'], ["This field is required."]
        )
        self.assertEqual(
            form.errors['password'], ["This field is required."]
        )

    def test_register_event_form(self):
        event = eventModel.objects.create(title = "event", description="this is event",date =timezone.now(),place = "Fasilkom",image_url ="https://nani.png" )  
        response = Client().post('/register/1/event/', {'email' : 'yosafattri@gmail.com', 'username' : 'yosafat', 'password' : 'yosafat'})
        self.assertEqual(eventModel.objects.all().count(),1)

    def test_register_event_form_nonmember(self):
        event = eventModel.objects.create(title = "event", description="this is event",date =timezone.now(),place = "Fasilkom",image_url ="https://nani.png" )  
        response = Client().post('/register/1/event/nonMember/', {'email' : 'yosafattri@gmail.com', 'username' : 'yosafat', 'password' : 'yosafat'})
        self.assertEqual(nonMemberModel.objects.all().count(), 1)
        