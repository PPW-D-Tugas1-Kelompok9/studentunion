from django import forms
from .models import memberModel
import datetime

class registerMemberForm(forms.ModelForm):
    class Meta:
        model = memberModel
        fields = '__all__'
        fields_required = '__all__'
        widgets = {
            'firstName': forms.TextInput(attrs={'class': 'form-control', 'placeholder': "First Name"}),
            'lastName': forms.TextInput(attrs={'class': 'form-control', 'placeholder': "Last Name"}),
            'birth': forms.DateInput(attrs={'type': 'text', 'onfocus': "(this.type = 'date')", 'class': 'form-control', 'placeholder': "Date of Birth"}),
            'email': forms.EmailInput(attrs={'class': 'form-control', 'placeholder': "Email"}),
            'address': forms.TextInput(attrs={'class': 'form-control', 'placeholder': "Home Address"}),
            'username': forms.TextInput(attrs={'class': 'form-control', 'placeholder': "Username"}),
            'password': forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': "Password"}),
        }

    passwordAgain = forms.CharField(label = '', widget = forms.PasswordInput(attrs={'class': 'form-control', 
        'placeholder': "Password (Again)"}))

    def clean(self):
        cleaned_data = super(registerMemberForm, self).clean()
        birth = cleaned_data.get('birth')
        password = cleaned_data.get('password')
        passwordAgain = cleaned_data.get('passwordAgain')
        if (datetime.datetime.now().date() < birth):
            self.add_error('birth', "Date of birth can't be in the future!")
        if (len(password) < 8):
            self.add_error('password', "Password must be at least 8 characters long")
        elif (password.isalnum() == False):
            self.add_error('password', "Password must only contains letters or numbers")
        elif (password != passwordAgain):
            self.add_error('password', "The two password fields don't match!")
        return cleaned_data