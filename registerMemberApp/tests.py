from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, saveMember
from .models import memberModel
from .forms import registerMemberForm
from django.http import HttpRequest
from datetime import datetime, timedelta
import unittest

# Create your tests here.
class registerMemberUnitTest(TestCase):
	def test_register_member_url_is_exist(self):
		response = Client().get('/registerMember/')
		self.assertEqual(response.status_code, 200)

	def test_register_member_using_index_func(self):
		found = resolve('/registerMember/')
		self.assertEqual(found.func, index)

	def test_index_contains_Register_Member(self):
		request = HttpRequest()
		response = index(request)
		html_response = response.content.decode('utf8')
		self.assertIn('REGISTER MEMBER', html_response)

	def test_model_can_create_new_member(self):
		member = memberModel.objects.create(firstName = 'Diana', lastName = 'Sari',
			birth = "1999-01-01", email = 'dianasari@gmail.com', address = 'DKI Jakarta', 
			username = 'dianasari01', password = '01011999')
		count_all_member = memberModel.objects.all().count()
		self.assertEqual(count_all_member, 1)

	def test_form_validation_for_blank_items(self):
		form = registerMemberForm(data = {'firstName': '', 'lastName': '', 'birth': '1999-01-01', 
			'email': '', 'address': '', 'username': '', 'password': '12345678', 'passwordAgain': '12345678'})
		self.assertFalse(form.is_valid())
		self.assertEqual(
			form.errors['firstName'],
			["This field is required."]
		)

	def test_form_validation_for_date_of_birth_is_future_date(self):
		date = datetime.now().date() + timedelta(days=1)
		response_post = Client().post('/registerMember/saveMember', {'firstName': 'Diana', 'lastName': 'Sari', 
			'birth': date, 'email': 'dianasari@gmail.com', 'address': 'DKI Jakarta', 
			'username': 'dianasari01', 'password': '1', 'passwordAgain': '1'})
		self.assertEqual(response_post.status_code, 200)

		response= Client().get('/registerMember/')
		html_response = response.content.decode('utf8')
		self.assertNotIn('Thank you for registering', html_response)

	def test_form_validation_for_password_less_than_8_characters(self):
		response_post = Client().post('/registerMember/saveMember', {'firstName': 'Diana', 'lastName': 'Sari', 
			'birth': '1999-01-01', 'email': 'dianasari@gmail.com', 'address': 'DKI Jakarta', 
			'username': 'dianasari01', 'password': '1', 'passwordAgain': '1'})
		self.assertEqual(response_post.status_code, 200)

		response= Client().get('/registerMember/')
		html_response = response.content.decode('utf8')
		self.assertNotIn('Thank you for registering', html_response)

	def test_form_validation_for_password_contains_non_alphanumeric_characters(self):
		response_post = Client().post('/registerMember/saveMember', {'firstName': 'Diana', 'lastName': 'Sari', 
			'birth': '1999-01-01', 'email': 'dianasari@gmail.com', 'address': 'DKI Jakarta', 
			'username': 'dianasari01', 'password': '1234567*', 'passwordAgain': '1234567*'})
		self.assertEqual(response_post.status_code, 200)

		response= Client().get('/registerMember/')
		html_response = response.content.decode('utf8')
		self.assertNotIn('Thank you for registering', html_response)

	def test_form_validation_for_2_password_fields_dont_match(self):
		response_post = Client().post('/registerMember/saveMember', {'firstName': 'Diana', 'lastName': 'Sari', 
			'birth': '1999-01-01', 'email': 'dianasari@gmail.com', 'address': 'DKI Jakarta', 
			'username': 'dianasari01', 'password': '12345678', 'passwordAgain': '12345679'})
		self.assertEqual(response_post.status_code, 200)

		response= Client().get('/registerMember/')
		html_response = response.content.decode('utf8')
		self.assertNotIn('Thank you for registering', html_response)

	def test_new_member_success_and_render_the_result(self):
		response_post = Client().post('/registerMember/saveMember', {'firstName': 'Diana', 'lastName': 'Sari', 
			'birth': '1999-01-01', 'email': 'dianasari@gmail.com', 'address': 'DKI Jakarta', 
			'username': 'dianasari01', 'password': '01011999', 'passwordAgain': '01011999'})
		self.assertEqual(response_post.status_code, 302)

		response= Client().get('/registerMember/registerSuccess/')
		html_response = response.content.decode('utf8')
		self.assertIn('Thank you for registering', html_response)

	def test_new_member_error_and_render_the_result(self):
		response_post = Client().post('/registerMember/saveMember', {'firstName': 'Diana' * 100, 'lastName': 'Sari', 
			'birth': '1999-01-01', 'email': 'dianasari@gmail.com', 'address': 'DKI Jakarta', 
			'username': 'dianasari01', 'password': '01011999', 'passwordAgain': '01011999'})
		self.assertEqual(response_post.status_code, 200)

		response= Client().get('/registerMember/')
		html_response = response.content.decode('utf8')
		self.assertNotIn('Thank you for registering', html_response)