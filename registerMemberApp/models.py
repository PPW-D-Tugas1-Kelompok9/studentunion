from django.db import models

class memberModel(models.Model):
    firstName = models.CharField(max_length = 20, verbose_name='')
    lastName = models.CharField(max_length = 20, verbose_name='')
    birth = models.DateField(verbose_name='')
    email = models.EmailField(unique = True, verbose_name='', 
    	error_messages={'unique':"This email has already been registered."})
    address = models.CharField(max_length = 30, verbose_name='')
    username = models.CharField(max_length = 15, unique = True, verbose_name='', 
    	error_messages={'unique':"This username has already been registered."})
    password = models.CharField(max_length = 20, verbose_name='')