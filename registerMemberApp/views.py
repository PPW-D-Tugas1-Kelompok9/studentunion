from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import memberModel
from .forms import registerMemberForm

response = {}
def index(request):
    members = memberModel.objects.all()
    response['members'] = members
    response['form'] = registerMemberForm
    return render(request, 'register_member.html', response)

def registerSuccess(request):
    return render(request, 'register_success.html', response)

def saveMember(request):
    form = registerMemberForm(request.POST or None)
    if (request.method == "POST" and form.is_valid()):
        response['firstName'] = request.POST['firstName']
        response['lastName'] = request.POST['lastName']
        response['birth'] = request.POST['birth']
        response['email'] = request.POST['email']
        response['address'] = request.POST['address']
        response['username'] = request.POST['username']
        response['password'] = request.POST['password']
        response['passwordAgain'] = request.POST['passwordAgain']
        member = memberModel(firstName = response['firstName'], lastName = response['lastName'],
            birth = response['birth'], email = response['email'], address = response['address'], 
            username = response['username'], password = response['password'])
        member.save()
        return HttpResponseRedirect('/registerMember/registerSuccess/')
    else: 
       return render(request, 'register_member.html', {'form':form})