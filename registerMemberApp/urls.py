from django.conf.urls import url
from .views import index, saveMember, registerSuccess

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^registerSuccess', registerSuccess, name='registerSuccess'),
    url(r'^saveMember', saveMember, name='saveMember'),
]
