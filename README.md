# Repository Tugas 1 PPW-D (kelompok 9)

## Anggota kelompok
- Ariq Naufal Satria : 1706027414
- Chelsy Lifardy : 1706043582
- Julia Ningrum : 1706979322
- Yosafat Tri Putra Brata : 1706043430

## Status Website
[![pipeline status](https://gitlab.com/PPW-D-Tugas1-Kelompok9/studentunion/badges/master/pipeline.svg)](https://gitlab.com/PPW-D-Tugas1-Kelompok9/studentunion/commits/master)
[![coverage report](https://gitlab.com/PPW-D-Tugas1-Kelompok9/studentunion/badges/master/coverage.svg)](https://gitlab.com/PPW-D-Tugas1-Kelompok9/studentunion/commits/master)

## Link Website Herokuapp
[https://fasilkom-student-union.herokuapp.com/](https://fasilkom-student-union.herokuapp.com/)

## Pembagian Tugas
- Ariq Naufal Satria : 
- Chelsy Lifardy : 
- Julia Ningrum : 
- Yosafat Tri Putra Brata : 

---
## Catatan Kelompok

### Git Flow
- Git clone `git clone https://gitlab.com/PPW-D-Tugas1-Kelompok9/studentunion.git`

- Bikin branch baru dari master pake `git checkout -b [nama branch]`

- Kalau udah commit dan push dulu ke branch sendiri sendiri `git push origin [nama branch]`

- Merge request ke master kalau udah, caranya liat di web gitlab nya --> merge request, atau kita chat di grup biar ga ada conflict wkwk.

- Kalau pengen ada perubahan lagi pastiin branch di lokal sekarang di branch kalian. Caranya nge-ceknya `git branch`. 

- Kalau mau pindah ke branch (yang udah ada) `git checkout [nama branch]`.

- Pull ulang dari master `git pull origin master` biar up-to-date dengan berbagai update dari yang lain, edit edit dan lakukan apapun, kembali ke step 3* (commit and push to branch and merge request).

- Cara delete local branch `git branch -d [nama branch]`.

- Cara delete remote branch (branch di gitlabnya) selain cara manual itu `git push [remote] -d [nama branch]`. contoh  `git push origin -d issue`

- **Sebelum pindah branch, usahakan commit dulu.. sebenarnya bisa pake `stash` sih tapi biar gampang commit aja**

- **Rajin rajin pull dari master sebelum ngoding**

[CheatSheet Git](https://www.git-tower.com/blog/git-cheat-sheet)

### Cara Tidak *Best Practice* untuk Merge*
- Setelah dari tahap commit dan push ke branch di gitlab, pindah ke master di lokal `git checkout master`

- lalu merge di lokal `git merge [nama branch]`

- Kalau ada conflict solve dulu , chat di grup biar aman wkwk

- Kalau ga ada conflict, langsung aja push ke master `git push origin master`

---


### Python
- Nama class `CamelCase` atau `snake_case`
- Nama variable `[namaClass_namaVariable]` biar gak bingung pas bikin di models nya contoh `Models_Event`
- Nama method nya `[nama class]_namaMethod` contoh `View_Home`

### HTML-CSS
- Class HTML `[namaHTML_class_subclass]` contoh `home_col_kolomEvent` biar gampang pas style CSS nyas