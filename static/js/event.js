$(document).ready(function () {
	$(".btn-join").click(function () {
		var id = $(this).attr('event-id');
		$.ajax({
			type: 'GET',
			url: '/profile/check_user',
			dataType: 'json',
		}).then(function (response) {
			if (response.login === "true") {
				$.ajax({
					url: '/check_user_is_registered',
					method: 'GET',
					data: { "event_id": id },
					dataType: 'json'
				}).then(function (response) {
					if (response.registered) {
						$(".event-modal-title").text("Register Failed");
						$(".event-modal-body-text").text("You have already joined the event!");
						$('#templateModal').modal('show');
					} else {
						$.ajax({
							url: '/join_event',
							method: 'GET',
							data: { "event_id": id },
							dataType: 'json'
						}).then(function (response) {
							if (response.success) {
								$(".event-modal-title").text("Register Success");
								$(".event-modal-body-text").text("You have successfully joined the event!");
								$('#templateModal').modal('show');
							}
						});
					}
				});
			} else {
				$('#loginRequirementModal').modal('show');
			}
		});
	});

	$("#loginBtn").click(function () {
		$('#loginRequirementModal').modal('hide');
		$('#ModalLogin').modal('show');
	});
});
