$('body').onload = loadData();

function loadData() {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();

    $.ajax({
        method: 'POST',
        url: '/profile/render/',
        headers: {
            "X-CSRFToken": csrftoken,
        },
        success: function (result) {
            var data = result.data
            var counter = result.counter
            console.log(data)
            for (var i = 0; i < data.length; i++) {
                console.log(data[i]);
                var image = `<div id="event-img" class="text-center">
                <img src = "${data[i]['image_url']}"></div>`
                var desc_1 = `<h4 class="font-weight-bold">${data[i]['title']}</h4>
                <p class="font-weight-bold"> Tanggal : `;
                var desc_2 = `</p> <p class="font-weight-bold"> Tempat : ${data[i]['place']} </p>`
                var dateString = `${data[i]['date']}`;
                var d = new Date(dateString);
                var date = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
                var tag_open = `<div class="event text-justify">
                <div class="event-top">`;
                var tag_close = `</div>
                </div>`;
                $('#event').append(tag_open + image + desc_1 + date + desc_2 + tag_close);
            }
            $('#counter-register').append(counter);
        }
    });
}

