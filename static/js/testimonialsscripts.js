function postTesti(event){
    event.preventDefault(true)
    fetch('https://fasilkom-student-union.herokuapp.com/about/', {
      method: 'post',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'X-CSRFToken': document.getElementsByName('csrfmiddlewaretoken')[0].value,
        'X-Requested-With': 'XMLHttpRequest',
      },
      body:JSON.stringify({ testimony : document.getElementById("testimonials-field").value })
    }).then(function(response) {
        return response.json();
    }).then(function(data){
        let wrapper = document.getElementById("testimony-wrapper")
        let inputDiv = document.createElement("div")
        let stylingInput =  document.createElement("strong")
        let input = document.createElement("p")
        input.classList.add("testimonials-style")
        input.innerText = document.getElementById("testimonials-field").value
        stylingInput.appendChild(input)
        inputDiv.appendChild(stylingInput)
        wrapper.prepend(inputDiv)
    }).catch(err => console.error(err));
}