from django.db import models

class newsModel(models.Model):
    writer = models.CharField(max_length=50)
    title = models.CharField(max_length=50)
    tag = models.CharField(max_length=20)
    image_url = models.CharField(max_length=100,null=True)
    date = models.DateTimeField()
    content = models.TextField()

    def __str__(self):
        return self.title
# Create your models here.
