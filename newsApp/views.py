from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import *
from .forms import *

response = {}
# Create your views here.
def view_news(request):
    response = {'newslist' : newsModel.objects.all()}
    return render(request,'news.html',response)

def view_news_article(request,id,news_title):
    response ={'news_list' : newsModel.objects.filter(id=id)}
    return render(request,'news_article.html',response)

# def filtering_news(request):
#     if request.method == 'POST':
#         form = filterForm(request.POST)
#
#     if form.is_valid():
#         answer = form.cleaned_data['tag_value']
#
#     response = {'news_list': newsModel.objects.filter(title=answer)}
#     return HttpResponseRedirect('news/',response)