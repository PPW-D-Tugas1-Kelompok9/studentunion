from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.utils import timezone
from .views import *
from .models import *

# Create your tests here.
class newsApp_test(TestCase):

    def setUp(self):
        return newsModel.objects.create(writer='Arnastria',title='Article Title',
                                        tag='Article',date=timezone.now(),content='This is the content')

    def test_newsApp_url_routing(self):
        response = Client().get('/news/')
        self.assertEqual(response.status_code, 200)

    def test_newsApp_correct_view(self):
        response = resolve('/news/')
        self.assertEqual(response.func, view_news)

    def test_newsApp_randomArticle_handle404_redirect(self):
        response = Client().get('/news/1/naninani')

        self.assertEqual(response.status_code, 301)

    def test_model_can_print(self):
        new_model = self.setUp()
        self.assertEqual('Article Title', new_model.__str__())

    def test_model_content_is_valid(self):
        new_model = self.setUp()
        self.assertEqual(new_model.writer, 'Arnastria')
        self.assertEqual(new_model.tag, 'Article')

    def test_news_article_view(self):
        new_model = self.setUp()
        response = Client().get('/news/1/Article Title/')

        self.assertEqual(response.status_code, 200)

