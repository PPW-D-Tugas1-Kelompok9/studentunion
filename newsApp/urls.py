from django.urls import re_path,path
from .views import *


#url for app
urlpatterns = [
    path('<int:id>/<str:news_title>/', view_news_article),
    path('', view_news, name = 'news_page'),
]
